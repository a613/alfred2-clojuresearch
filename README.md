## Summary

This workflow is an "I'm feeling lucky" search for clojuredocs.org, written as an Alfred 2 workflow. Before using it, you must install the free Mac [Alfred](https://www.alfredapp.com/) app and activate the [powerpack](https://www.alfredapp.com/powerpack/) (paid addon to Alfred). Once Alfred is installed, download the latest release of this workflow and double-click it to add it to Alfred.

Results are cached indefinitely per query and will be returned even when offline, but cannot be preloaded in anticipation of non-connectivity.

## Usage

On the first query, dependencies will need to be (automatically!) installed, so repeat your query after waiting 10-15 seconds, by which point things should be set up. This is all taken care of by the excellent Bundler script (see below).

With no arguments: "**cj**":

* Opens clojuredocs.org in the default browser.

* Keyword can be configured in Alfred configuration interface.

With argument: "**cj ???**":

* Lists matching functions, macros, and vars found on clojuredocs.org.

* Opens the corresponding docs page in the default browser when one is selected.

* Keyword can be configured in Alfred configuration interface.

System-wide hotkey: "suggested **Shift-Command-D**":

* Lists matching functions, macros, and vars found on clojuredocs.org.

* Opens the corresponding docs page in the default browser when one is selected.

* Hotkey can be configured in Alfred configuration interface.

## Credits

Some icons or parts of icons were made by [Freepik](http://www.flaticon.com/authors/freepik) from [www.flaticon.com](http://www.flaticon.com) under the [Creative Commons BY 3.0](http://creativecommons.org/licenses/by/3.0/) license.

Some icons or parts of icons were made by [Font Awesome](http://fortawesome.github.io/Font-Awesome/).

Some icons or parts of icons were made by an [unknown party](http://verse.aasemoon.com/images/5/51/Clojure-Logo.png).

Bundler was obtained from [packal.org](http://www.packal.org/workflow/alfred-dependency-bundler-demo-python).

Workflow was obtained from [deanishe on GitHub](https://github.com/deanishe/alfred-workflow).

The edn_format package  was obtained from [Swaroop C H](https://pypi.python.org/pypi/edn_format).

This workflow would not function at all without the reliable website at [clojuredocs.org](http://clojuredocs.org). All the hard work has already been done; I just made it available through Alfred.

## Bug Reporting

Please report any bugs, questions, and ideas through the [alfred2-clojuresearch Bitbucket repo](https://bitbucket.org/trimtab613/alfred2-clojuresearch/issues)

## Notes

This has been developed and only tested with OS X Yosemite 10.10.